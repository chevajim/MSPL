---
title: "Projet.espec"
author: "CHEVALLIER Jimmy"
date: "13/03/2018"
output: pdf_document
---

MS Mini-Projet

STUDENTS GROUP: CHEVALLIER Jimmy, KOC Mickael


Table of contents:

## I Introduction
###  Context / Dataset description
#### Chargement du fichier
#### Spécifications

###  How the dataset has been obtained?

###  Description of the question

## II Methodology
###  Data clean-up procedures
#### Etude plus detaillé de nos donnée

## III Analysis in Literate Programming :Scientific workflow
#### 1 Classement de la consomation de tabac en EUROPE (Par pays)
#### 2 Consomation de tabac en Europe 
#### 3 Repartition Homme et Femme de la consomation de tabac en EUROPE sur la population de chaque sexe respectif 
#### 4 Repartition de la consomation de tabac par tranche d'age
#### 5 Proportion de fumeur en fonction du niveau d'etude
#### 6 Repartition des fumeurs par niveau de consomation en Europe
#### 7 Classement des profils de fumeurs les plus présent en Europe

## IV Conclusion

## V Bonus

# I Introduction

## Context / Dataset description

  Source: http://ec.europa.eu/eurostat/estat-navtree-portlet-prod/BulkDownloadListing?file=data/hlth_ehis_sk1e.tsv.gz&unzip=true
  
Ce jeu de données met en oeuvre le modèle sur l'indice de consommation de cigarettes en Europe et par pays en "%" de population. Selon des données propres à l'année 2014, un recensement des caractéristiques suivantes : 
le sexe l'âge le niveau d'études le niveau de consommation la localisation
Ceux-ci vont nous permettre d'étudier et observer s'il y a des corelations entre ses différents facteurs qui viendront s'ajouter au contexte actuel de l'époque. L'arrivée de nouveaux outils de consommation aux yeux du grand public comme la cigarette électronique aura elle aussi une part de responsabilité mais celle-ci ne sera pas l'objet de notre étude. 

Informations supplémentaires (source: http://ec.europa.eu/eurostat/web/products-datasets/-/hlth_ehis_sk1e)
Dernière mise à jour des données: 20.03.17 
Les données les plus anciennes: 2014 
Les données les plus récentes: 2014 
Nombre de valeurs: 34848

### Chargement du fichier :
```{r}
library(dplyr);
library(ggplot2);
library(stringr);

df <- read.csv("tabac.tsv", header=TRUE, sep = "\t")
```

Voici les différentes variables sur lesquels nous allons travailler :
```{r}
names(df)
```

### Spécifications:


"smoking" --> type de consomation

NSM: Non smoker

SM_CUR: Current smoker (fumeur actuel) (non utilisé dans notre etude)

SM_DAY: Daily smoker (fume tout les jours)

SM_OCC: Occasional smoket (fumeur occasionel)

Un fumeur quotidien (ou « régulier ») est une personne qui déclare fumer tous les jours ou fumer au moins 1 cigarette par jour. Le fumeur occasionnel est quant à lui une personne qui fume moins d'une cigarette par jour.

"isced11" --> Niveau d'étude

ED0-2: ISCED levels 0-2 (Pre-primary, primary and lower secondary education)

ED3_4: ISCED levels 3 and 4 (Upper secondary and post-secondary non-tertiary education)

ED5-8: ISCED levels 5-8 (Tertiary education)


"sexe" -->

F = Femme
H = Homme
T = Total

"age" -->

Y_GE18 = 18 ans ou plus
Y15-24 = De 15 à 24 ans
..

"EU-28" --> it is the abbreviation of European Union (EU) which consists a group of 28 countries (Belgium, Bulgaria, Czech Republic, Denmark, Germany, Estonia, Ireland, Greece, Spain, France, Croatia, Italy, Cyprus, Latvia, Lithuania, Luxembourg, Hungary, Malta, Netherlands, Austria, Poland, Portugal, Romania, Slovenia, Slovakia, Finland, Sweden, United Kingdom).


"u" : low reliability or no value

"e" :	estimé
Le fichier contient 1104 observations.

## How the dataset has been obtained?

Après concertation et quelques recherches, nous nous sommes interrogé sur le tabagisme. Au-delà de la question: les femmes sont-elles plus "fumeur" que les hommes?, qui revient assez régulièrement sur les forums et autres plateformes de communications, nous souhaitions aller au-delà et prendre en compte d'autres paramètres. De plus un phénomène troublant nous à interpeler concernant les fumeurs qui sont de plus en plus jeunes et cette image en tête du collégien cigarette à la main. Nous avons donc orienté nos recherches en conséquence pour finalement arriver au fichier présenté précédemment.

##   Description of the question

Problématique : L'âge, le sexe, le niveau d'études et la localisation ont-ils un impact sur la consommation de cigarettes en Europe? (donnée sur le 2014)

Nous distinguerons donc des profils de fumeurs suivant une démarche décrite ci-dessous.

# II Methodology

## Data clean-up procedures
  
  Pour commencer nos donnée n'était au debut pas exploitables car certains champs étaient liés et rendais une étude impossible --> Source: http://ec.europa.eu/eurostat/estat-navtree-portlet-prod/BulkDownloadListing?file=data/hlth_ehis_sk1e.tsv.gz&unzip=true

Nous avons dans un premier temps séparé certains champs en remplacent certains séparateurs afin de pouvoir dissocier les champs "unissent", "smoking", "isced11", "sex", "âge", "time \ Geo".

De plus comme indiqués dans la spécification nous pouvons observer la valeur "u" indiqué dans certaines cellules qui signifie une fiabilité de l'information très faible voire même information inexistante.

###  En étudiant plus en details nos donnée on compte:
  
  - 4208 valeurs imprecises ou inexistantes dont 1675 inexistantes
  
  - 34910 valeurs au total
  
  => donc environ 12%
  
```{r}

#Creation d une matrice qui va contenir nos valeur de comptage par pays
matrice<-matrix(nrow=0, ncol=1,byrow=T)
colnames(matrice)<-c("count")

#Creation de variable de comptage
#Ajout de valeur a notre matrice
df %>%
  select(BE) %>%
  filter(grepl("u", BE, fixed = TRUE)) %>%
  count()-> BEcount
  matrice<-rbind(matrice, BE = c(BEcount))

df %>%
  select(BG) %>%
  filter(grepl("u", BG, fixed = TRUE)) %>%
  count()->BGcount
  matrice<-rbind(matrice, BG = c(BGcount))

df %>%
  select(CZ) %>%
  filter(grepl("u", CZ, fixed = TRUE)) %>%
  count()->CZcount
  matrice<-rbind(matrice, CZ = c(CZcount))

df %>%
  select(DK) %>%
  filter(grepl("u", DK, fixed = TRUE)) %>%
  count()->DKcount
  matrice<-rbind(matrice, DK = c(DKcount))


df %>%
  select(DE) %>%
  filter(grepl("u", DE, fixed = TRUE)) %>%
  count()->DEcount
  matrice<-rbind(matrice, DE = c(DEcount))

df %>%
  select(EE) %>%
  filter(grepl("u", EE, fixed = TRUE)) %>%
  count()->EEcount
  matrice<-rbind(matrice, EE = c(EEcount))

df %>%
  select(IE) %>%
  filter(grepl("u", IE, fixed = TRUE)) %>%
  count()->IEcount
  matrice<-rbind(matrice, IE = c(IEcount))

df %>%
  select(EL) %>%
  filter(grepl("u", EL, fixed = TRUE)) %>%
  count()->ELcount
  matrice<-rbind(matrice, EL = c(ELcount))

df %>%
  select(ES) %>%
  filter(grepl("u", ES, fixed = TRUE)) %>%
  count()->EScount
  matrice<-rbind(matrice, ES = c(EScount))

df %>%
  select(FR) %>%
  filter(grepl("u", FR, fixed = TRUE)) %>%
  count()->FRcount
  matrice<-rbind(matrice, FR = c(FRcount))

df %>%
  select(HR) %>%
  filter(grepl("u", HR, fixed = TRUE)) %>%
  count()->HRcount
  matrice<-rbind(matrice, HR = c(HRcount))


df %>%
  select(IT) %>%
  filter(grepl("u", IT, fixed = TRUE)) %>%
  count()->ITcount
  matrice<-rbind(matrice, IT = c(ITcount))

df %>%
  select(CY) %>%
  filter(grepl("u", CY, fixed = TRUE)) %>%
  count()->CYcount
  matrice<-rbind(matrice, CY = c(CYcount))

df %>%
  select(LV) %>%
  filter(grepl("u", LV, fixed = TRUE)) %>%
  count()->LVcount
  matrice<-rbind(matrice, LV = c(LVcount))


df %>%
  select(LT) %>%
  filter(grepl("u", LT, fixed = TRUE)) %>%
  count()->LTcount
  matrice<-rbind(matrice, LT = c(LTcount))

df %>%
  select(LU) %>%
  filter(grepl("u", LU, fixed = TRUE)) %>%
  count()->LUcount
  matrice<-rbind(matrice, LU = c(LUcount))

df %>%
  select(HU) %>%
  filter(grepl("u", HU, fixed = TRUE)) %>%
  count()->HUcount
  matrice<-rbind(matrice, HU = c(HUcount))

df %>%
  select(MT) %>%
  filter(grepl("u", MT, fixed = TRUE)) %>%
  count()->MTcount
  matrice<-rbind(matrice, MT = c(MTcount))


df %>%
  select(NL) %>%
  filter(grepl("u", NL, fixed = TRUE)) %>%
  count()->NLcount
  matrice<-rbind(matrice, NL = c(NLcount))

df %>%
  select(AT) %>%
  filter(grepl("u", AT, fixed = TRUE)) %>%
  count()->ATcount
  matrice<-rbind(matrice, AT = c(ATcount))

df %>%
  select(PL) %>%
  filter(grepl("u", PL, fixed = TRUE)) %>%
  count()->PLcount
  matrice<-rbind(matrice, PL = c(PLcount))

df %>%
  select(PT) %>%
  filter(grepl("u", PT, fixed = TRUE)) %>%
  count()->PTcount
  matrice<-rbind(matrice, PT = c(PTcount))


df %>%
  select(RO) %>%
  filter(grepl("u", RO, fixed = TRUE)) %>%
  count()->ROcount
  matrice<-rbind(matrice, RO = c(ROcount))

df %>%
  select(SI) %>%
  filter(grepl("u", SI, fixed = TRUE)) %>%
  count()->SIcount
  matrice<-rbind(matrice, SI = c(SIcount))

df %>%
  select(SK) %>%
  filter(grepl("u", SK, fixed = TRUE)) %>%
  count()->SKcount
  matrice<-rbind(matrice, SK = c(SKcount))

df %>%
  select(FI) %>%
  filter(grepl("u", FI, fixed = TRUE)) %>%
  count()->FIcount
  matrice<-rbind(matrice, FI = c(FIcount))


df %>%
  select(SE) %>%
  filter(grepl("u", SE, fixed = TRUE)) %>%
  count()->SEcount
  matrice<-rbind(matrice, SE = c(SEcount))

df %>%
  select(UK) %>%
  filter(grepl("u", UK, fixed = TRUE)) %>%
  count()->UKcount
  matrice<-rbind(matrice, UK = c(UKcount))

df %>%
  select(IS) %>%
  filter(grepl("u", IS, fixed = TRUE)) %>%
  count()->IScount
  matrice<-rbind(matrice, IS = c(IScount))

df %>%
  select(NO) %>%
  filter(grepl("u", NO, fixed = TRUE)) %>%
  count()->NOcount
  matrice<-rbind(matrice, NO = c(NOcount))

df %>%
  select(TR) %>%
  filter(grepl("u", TR, fixed = TRUE)) %>%
  count()->TRcount
  matrice<-rbind(matrice, TR = c(TRcount))

```
  
```{r}
countListe<-c(BEcount$n,BGcount$n,CZcount$n,DKcount$n,DEcount$n,EEcount$n,IEcount$n,ELcount$n,EScount$n,FRcount$n,HRcount$n,ITcount$n,CYcount$n,LVcount$n,LTcount$n,LUcount$n,HUcount$n,MTcount$n,NLcount$n,ATcount$n,PLcount$n,PTcount$n,ROcount$n,SIcount$n,SKcount$n,FIcount$n,SEcount$n,UKcount$n,IScount$n,NOcount$n,TRcount$n)

resultats<-data.frame(Pays=names(df)[8:38],Nb_Valeur_Imprécise = countListe)
resultats$rank <- rank(-resultats$Nb_Valeur_Imprécise)
# Classement des Pays avec le plus de valeur imprécise
head(resultats[ order(-resultats[,2]), ])

# Classement des Pays avec le moins de valeur imprécise
head(resultats[ order(resultats[,2]), ])

```
Avec ces observations on retient que les pays ayant le plus de valeur peu fiable sont BE*, MT, LU, FI, CZ (Est Europe) et les Pays les plus précis sont IT, FR, DE, PL, ES, UK (Ouest Europe).

Néanmoins un pays attire notre attention, la Belgique avec 1022 valeurs imprécises sur 1104 au total.

La question de supprimer ce pays de notre jeu de données de peur de fausser notre étude se pose. Nous y reviendrons durant l'étape suivante.

En résumé, cela nous permet donc de classifier en partie la nature de notre étude ainsi que la précision en fonction de la localisation (distinction des pays de l'Est et de l'Ouest de l'Europe).

## III Analysis in Literate Programming :Scientific workflow


Nous démarrons notre étude en établissant un classement de la consommation de tabac par Pays en Europe.


###Classement de la consomation de tabac en EUROPE (Par pays)

```{r}

#On selectionne uniquement les criteres total des nom fumeur 
dfwu <- read.csv("tabacWithoutU.tsv", header=TRUE, sep = "\t")


dfEuropeNSM <- subset(dfwu, grepl("NSM", df$smoking))
dfEuropeNSM <- subset(dfEuropeNSM, grepl("TOTAL", df$isced11))
dfEuropeNSM <- subset(dfEuropeNSM, grepl("T", df$sex))
dfEuropeNSM <- subset(dfEuropeNSM, grepl("TOTAL", df$age))

#On retire les valeur NA generer avec la fct subset
dfEuropeNSM <- dfEuropeNSM[complete.cases(dfEuropeNSM), ]

#Operation 100% - NSM% = %_de_fumeur dans population total
dfEuropeNSM <-   mutate(dfEuropeNSM, CZ= 100-CZ)
dfEuropeNSM <-   mutate(dfEuropeNSM, DK= 100-DK)
dfEuropeNSM <-   mutate(dfEuropeNSM, DE= 100-DE)
dfEuropeNSM <-   mutate(dfEuropeNSM, EE= 100-EE)
dfEuropeNSM <-   mutate(dfEuropeNSM, IE= 100-IE)
dfEuropeNSM <-   mutate(dfEuropeNSM, EL= 100-EL)
dfEuropeNSM <-   mutate(dfEuropeNSM, ES= 100-ES)
dfEuropeNSM <-   mutate(dfEuropeNSM, FR= 100-FR)
dfEuropeNSM <-   mutate(dfEuropeNSM, HR= 100-HR)
dfEuropeNSM <-   mutate(dfEuropeNSM, IT= 100-IT)
dfEuropeNSM <-   mutate(dfEuropeNSM, CY= 100-CY)
dfEuropeNSM <-   mutate(dfEuropeNSM, LV= 100-LV)
dfEuropeNSM <-   mutate(dfEuropeNSM, LT= 100-LT)
dfEuropeNSM <-   mutate(dfEuropeNSM, BE= 100-66)
dfEuropeNSM <-   mutate(dfEuropeNSM, BG= 100-77)
dfEuropeNSM <-   mutate(dfEuropeNSM, LU= 100-LU)
dfEuropeNSM <-   mutate(dfEuropeNSM, HU= 100-HU)
dfEuropeNSM <-   mutate(dfEuropeNSM, MT= 100-MT)
dfEuropeNSM <-   mutate(dfEuropeNSM, NL= 100-NL)
dfEuropeNSM <-   mutate(dfEuropeNSM, AT= 100-AT)
dfEuropeNSM <-   mutate(dfEuropeNSM, PL= 100-PL)
dfEuropeNSM <-   mutate(dfEuropeNSM, PT= 100-PT)
dfEuropeNSM <-   mutate(dfEuropeNSM, RO= 100-RO)
dfEuropeNSM <-   mutate(dfEuropeNSM, SI= 100-SI)
dfEuropeNSM <-   mutate(dfEuropeNSM, SK= 100-SK)
dfEuropeNSM <-   mutate(dfEuropeNSM, FI= 100-FI)
dfEuropeNSM <-   mutate(dfEuropeNSM, SE= 100-SE)
dfEuropeNSM <-   mutate(dfEuropeNSM, UK= 100-UK)
dfEuropeNSM <-   mutate(dfEuropeNSM, IS= 100-IS)
dfEuropeNSM <-   mutate(dfEuropeNSM, NO= 100-NO)
dfEuropeNSM <-   mutate(dfEuropeNSM, TR= 100-TR)
dfEuropeNSM <-   mutate(dfEuropeNSM, EU28= 100-EU28)

dfEuropeNSMOnly <- dfEuropeNSM[ , c(7)]

dfEuropeNSM <- dfEuropeNSM[ , c(8:38)]

#Transposition de notre DataFrame pour pouvoir les trier 
dfEuropeNSM <- as.data.frame(t(dfEuropeNSM))
colnames(dfEuropeNSM)[colnames(dfEuropeNSM)=="V1"] <- "prct_de_fumeur"


paysl <- c("Belgique", "Bulgarie", "République tchèque", "Danemark", "Allemagne", "Estonie", "Irlande", "Grèce", "Espagne", "France", "Croatie", "Italie", "Chypre", "Lettonie", "Lituanie", "Luxembourg","Hongrie", "Malte", "Pays-Bas", "Autriche", "Pologne", "Portugal", "Roumanie", "Slovénie", "Slovaquie", "Finlande", "Suède", "Royaume-Uni", "Islande", "Norvège", "Turquie")
             
dfEuropeNSM$pays <- paysl


#Tri par ordre decroissant
index <- with(dfEuropeNSM, order(-prct_de_fumeur))
dfEuropeNSM<-data.frame(pays=dfEuropeNSM[0][index, ],prct_de_fumeur = dfEuropeNSM[1][index, ], pays = dfEuropeNSM[2][index, ])
dfEuropeNSM$rank <- rank(-dfEuropeNSM$prct_de_fumeur)


dfEuropeNSM$pays <- factor(dfEuropeNSM$pays, levels = dfEuropeNSM$pays[order(dfEuropeNSM$prct_de_fumeur)])

# Barplot basique
p<-ggplot(data=dfEuropeNSM, aes(x=pays, y=prct_de_fumeur)) +
  geom_bar(stat="identity", width=0.9)+
  geom_bar(stat="identity", fill="red")+
  theme_minimal()+ggtitle("Classement de la consomation de tabac en EUROPE (Par pays)")+
  scale_y_continuous(name="%Fumeur sur population total par pays") + scale_x_discrete(name="Pays") 

p + coord_flip()
```

Ici on peut voir la première place occupée par la Belgique qui est le 7 en plus gros consommateur de tabac au monde.
Et on voit que la Bulgarie se situe en milieu de tableau, elle n'est donc pas un extremum dans notre jeu de données. Nous choisirons donc de le garder dans notre étude.
Nous observons simplement une différence des taux en fonction du pays.

### Consomation de tabac en Europe sur population total
```{r }
# PC Fumeur europe
dfEuropeNSMOnly <- as.data.frame(t(dfEuropeNSMOnly))
dfEuropeNSMOnly<-data.frame(Pays=dfEuropeNSMOnly[0],prct_de_fumeur = dfEuropeNSMOnly[1])
#colnames(dfEuropeNSMOnly)[colnames(dfEuropeNSMOnly[0])] <- "Europeeee"
colnames(dfEuropeNSMOnly)[colnames(dfEuropeNSMOnly)=="V1"] <- "Europe"
row.names(dfEuropeNSMOnly)<-"%Fumeur sur population total"

dfEuropeNSMOnly

```
Nous avons donc 23,9% de fumeurs en Europe sur la population total.

### Repartition des fumeurs par niveau de consomation en Europe
```{r}
# Pourcentage de fumeur en fonction de leurs niveau d'étude
dfsm <- subset(dfwu, grepl("_D|_O", dfwu$smoking))
dfsm <- subset(dfsm, grepl("T", dfwu$sex))
dfsm <- subset(dfsm, grepl("TOTAL", dfwu$age))
dfsm <- subset(dfsm, grepl("TOTAL", dfsm$isced11))


dfsm <- dfsm[complete.cases(dfsm), ]

dfsm <- dfsm[ , c(2,7)]

index <- with(dfsm, order(-EU28))
dfsm<-data.frame(smoker_type=dfsm[1][index, ],EU28 = dfsm[2][index, ])
dfsm$rank <- rank(-dfsm$EU28)
#dfsm

dfsm %>%
  group_by(smoker_type) %>%
  summarize(PC_de_fumeur =EU28) %>%
  ggplot(aes(x=smoker_type, y=PC_de_fumeur))  + geom_bar(stat = "identity", fill = "steelblue")+ geom_text(aes(label=PC_de_fumeur), vjust=1.6, color="white", size=3.5)+theme_bw()+ggtitle("Repartition des fumeurs par niveau de consomation en Europe")+
  scale_y_continuous(name="%Fumeur sur population total") + scale_x_discrete(name="Type de consommation") 



```
On observe un écart d'environ 15% entre le taux de fumeurs quotidien et occasionnel. 

De plus nous pouvons vérifier en partie la coherence de notre jeu de données en retrouvant notre 23,9% (19.2+4.7 = 29.9) trouvé précédemment pour le taux de fumeur en Europe.

### Repartition Homme et Femme de la consomation de tabac en EUROPE sur la population de chaque sexe respectif
```{r }
dfAllCountry <-dfwu
dfAllCountry <- subset(dfAllCountry, grepl("_D", dfAllCountry$smoking))
dfAllCountry <- subset(dfAllCountry, grepl("M|F", dfAllCountry$sex))

dfAllCountry <- subset(dfAllCountry, grepl("TOTAL", dfAllCountry$isced11))

dfAllCountry <- subset(dfAllCountry, grepl("TOTAL", dfAllCountry$age))

#On retire les valeur NA generer avec la fct subset
dfAllCountry <- dfAllCountry[complete.cases(dfAllCountry), ]

dfAllCountry <- dfAllCountry[ , c(4, 8:38)]

dfAllCountry <- t(dfAllCountry)
dfAllCountry <- dfAllCountry[-1,]
#dfAllCountry <- dfAllCountry$col1


colnames(dfAllCountry) <- c("F","M")
#dfAllCountry

paysl <- c("Belgique", "Bulgarie", "République tchèque", "Danemark", "Allemagne", "Estonie", "Irlande", "Grèce", "Espagne", "France", "Croatie", "Italie", "Chypre", "Lettonie", "Lituanie", "Luxembourg","Hongrie", "Malte", "Pays-Bas", "Autriche", "Pologne", "Portugal", "Roumanie", "Slovénie", "Slovaquie", "Finlande", "Suède", "Royaume-Uni", "Islande", "Norvège", "Turquie")
  
#dfAllCountry[,1]
#dfAllCountry[,2]

dfAllCountry <- data.frame(pays =paysl, F =  dfAllCountry[,1], H = dfAllCountry[,2])


p1 <- ggplot(data=dfAllCountry, aes(x = pays)) + 
geom_line(aes(x=pays,y=H, color = "H", group = 1)) +
geom_line(aes(x=pays,y=F, color = "F", group = 2)) +

geom_point(aes(x=pays,y=H, color = "H", group = 1))+
geom_point(aes(x=pays,y=F, color = "F", group = 2))+
    theme(axis.text.x = element_text(face="bold", 
                           size=8, angle=90),
          axis.text.y = element_text(size = 8))+ggtitle("Repartition H/F de la consomation de tabac quotiedienne en EUR")+
  scale_y_discrete(name="%Fumeur de population total par sexe", breaks=c("33.0","16.0","10.3"),labels=c("33.0%", "16.0%", "10.3%")) +scale_x_discrete(name="Pays") 
p1
```
On observe des écarts plus ou moins importants entre les sexes en fonction du pays avec certains pays qui comportent des valeurs très faibles pour les Femmes comme la Lituanie, la Roumanie mais aussi la Suède qui a la particularité d'avoir quasiment la même proportion d'Hommes que de Femme fumeurs.

Le phonème observé nous indique donc que chaque pays a son influence (culturel, politiques ou autres) sur la consommation de cigarettes.

### Repartition Homme et Femme de la consomation de tabac en EUROPE sur la population de chaque sexe respectif

```{r}
dfHFEU <- subset(dfwu, grepl("NSM", dfwu$smoking))
dfHFEU <- subset(dfHFEU, grepl("TOTAL", dfwu$isced11))
dfHFEU <- subset(dfHFEU, grepl("TOTAL", dfwu$age))
dfHFEU <- subset(dfHFEU, grepl("M|F", dfHFEU$sex))
dfHFEU <-   mutate(dfHFEU, EU28= 100-EU28)

dfHFEU <- dfHFEU[complete.cases(dfHFEU), ]

dfHFEU <- dfHFEU[ , c(4,7)]

dfHFEU %>%
  group_by(sex) %>%
  summarize(PC_de_fumeur_par_Sexe =EU28) %>%
  ggplot(aes(x=sex, y=PC_de_fumeur_par_Sexe)) + geom_bar(stat = "identity", fill = "steelblue")+ geom_text(aes(label=PC_de_fumeur_par_Sexe), vjust=1.6, color="white", size=3.5)+theme_bw()+ggtitle("Repartition Homme et Femme de la consomation de tabac en EUROPE")+
  scale_y_continuous(name="%Fumeur sur population total par sexe") + scale_x_discrete(name="Sexe") 

library(prettyR)


```

On constate un écart d'environ 10 % entre les deux. Nous pouvons en déduire que les Hommes sont plus consommateur de cigarette que les femmes en Europe.

### Repartition de la consomation de tabac par tranche d'age

```{r}
dfageEU <- subset(dfwu, grepl("NSM", dfwu$smoking))
dfageEU <- subset(dfageEU, grepl("TOTAL", dfwu$isced11))
dfageEU <- subset(dfageEU, grepl("T", dfwu$sex))
dfageEU <- subset(dfageEU, grepl("_|-", dfwu$age))

dfageEU <-   mutate(dfageEU, EU28= 100-EU28)

dfageEU <- dfageEU[complete.cases(dfageEU), ]

dfageEU <- dfageEU[ , c(5,7)]

index <- with(dfageEU, order(-EU28))
dfageEU<-data.frame(age=dfageEU[1][index, ],EU28 = dfageEU[2][index, ])
dfageEU$rank <- rank(-dfageEU$EU28)
dfageEU

```
On observe que certaines tranches sont à cheval sûr d'autres (25-29 / 25-34). Nous représentons donc sous forme de tableau le classement des tranches d'âge et observons que la tranche d'âge la plus présente est la tranche 25-29.

Ce classement nous indique que la consommation de tabac est la plus importante pour les 18-30 puis diminue avec l’avancée en âge.

### Proportion de fumeur en fonction du niveau d'etude en Europe par Pays
```{r }
dfAllCountry <-dfwu
dfAllCountry <- subset(dfAllCountry, grepl("_D", dfAllCountry$smoking))
dfAllCountry <- subset(dfAllCountry, grepl("T", dfAllCountry$sex))

dfAllCountry <- subset(dfAllCountry, grepl("_|-", dfAllCountry$isced11))

dfAllCountry <- subset(dfAllCountry, grepl("TOTAL", dfAllCountry$age))

#On retire les valeur NA generer avec la fct subset
dfAllCountry <- dfAllCountry[complete.cases(dfAllCountry), ]

dfAllCountry <- dfAllCountry[ , c(3, 8:38)]
dfAllCountry
dfAllCountry <- t(dfAllCountry)
dfAllCountry <- dfAllCountry[-1,]
#dfAllCountry <- dfAllCountry$col1


colnames(dfAllCountry) <- c("ED0-2","ED3_4", "ED5-8")
dfAllCountry

paysl <- c("Belgique", "Bulgarie", "République tchèque", "Danemark", "Allemagne", "Estonie", "Irlande", "Grèce", "Espagne", "France", "Croatie", "Italie", "Chypre", "Lettonie", "Lituanie", "Luxembourg","Hongrie", "Malte", "Pays-Bas", "Autriche", "Pologne", "Portugal", "Roumanie", "Slovénie", "Slovaquie", "Finlande", "Suède", "Royaume-Uni", "Islande", "Norvège", "Turquie")
  
#dfAllCountry[,1]
#dfAllCountry[,2]

dfAllCountry <- data.frame(pays =paysl, ED0_2 =  dfAllCountry[,1], ED3_4 = dfAllCountry[,2], ED5_8 = dfAllCountry[,3])




p2<-ggplot(dfAllCountry) +
geom_line(aes(x=pays,y=ED0_2, color = "ED0_2", group = 1)) +
geom_line(aes(x=pays,y=ED3_4, color = "ED3_4", group = 2)) +
geom_line(aes(x=pays,y=ED5_8, color = "ED5_8", group = 3)) +
  
geom_point(aes(x=pays,y=ED0_2, color = "ED0_2", group = 1))+
geom_point(aes(x=pays,y=ED3_4, color = "ED3_4", group = 2))+
geom_point(aes(x=pays,y=ED5_8, color = "ED5_8", group = 3))+
    theme(axis.text.x = element_text(face="bold", 
                           size=8, angle=90),
          axis.text.y = element_text(size = 8)) +ggtitle("Proportion de fumeur en fonction du niveau d'etude en Europe par Pays")+
  scale_y_discrete(name="%Fumeur de population total", breaks=c("23.0","34.1","10.7"),labels=c("23.0%", "34.1%", "10.7%"))
p2

```
Ici nous observons, au delas des valeurs (comprises entre 5 et 30 %), que les variables ED0_2 et ED3_4 restes assez proche alors que la variable ED5_8 se situe la plupart du temps en dessous.

Cette première observation nous laisse supposer qu'un écart est réel entre certaines classes.

### Proportion de fumeur en fonction du niveau d'etude en Europe

```{r}
# Pourcentage de fumeur en fonction de leurs niveau d'étude
dflvlEtu <- subset(dfwu, grepl("NSM", dfwu$smoking))
dflvlEtu <- subset(dflvlEtu, grepl("T", dfwu$sex))
dflvlEtu <- subset(dflvlEtu, grepl("TOTAL", dfwu$age))
dflvlEtu <- subset(dflvlEtu, grepl("_|-", dflvlEtu$isced11))

dflvlEtu <-   mutate(dflvlEtu, EU28= 100-EU28)

dflvlEtu <- dflvlEtu[complete.cases(dflvlEtu), ]

dflvlEtu <- dflvlEtu[ , c(3,7)]

index <- with(dflvlEtu, order(-EU28))
dflvlEtu<-data.frame(lvl_Etude=dflvlEtu[1][index, ],EU28 = dflvlEtu[2][index, ])
dflvlEtu$rank <- rank(-dflvlEtu$EU28)
#dflvlEtu

dflvlEtu %>%
  group_by(lvl_Etude) %>%
  summarize(Prct_de_fumeur =EU28) %>%
  ggplot(aes(x=lvl_Etude, y=Prct_de_fumeur)) + geom_bar(stat = "identity", fill = "steelblue")+ geom_text(aes(label=Prct_de_fumeur), vjust=1.6, color="white", size=3.5)+theme_bw()+ggtitle("Proportion de fumeur en fonction du niveau d'etude en Europe")+
  scale_y_discrete(name="%Fumeur de population total par niveau d'étude") + scale_x_discrete(name="Niveau d'étude")

```
Même si l'analyse des relations entre tabagismes et caractéristiques sociodémographiques doit être interprétée avec prudence les résultats nous indique donc que les plus diplômés sont moins souvent fumeurs.

### Classement des profils de fumeurs les plus présent en Europe
```{r}

dfType <- subset(dfwu, grepl("_D|_O", dfwu$smoking))
dfType <- subset(dfType, grepl("M|F", dfType$sex))
dfType <- subset(dfType, grepl("_|-", dfType$isced11))
dfType <- subset(dfType, grepl("_|-", dfType$age))


dfType <- dfType[complete.cases(dfType), ]

dfType <- dfType[ , c(2:7)]

index <- with(dfType, order(-EU28))
dfType<-data.frame(Smoking=dfType[1][index, ],Isced11=dfType[2][index, ],Sex=dfType[3][index, ],Age=dfType[4][index, ],EU28 = dfType[6][index, ])
dfType$rank <- rank(-dfType$EU28)


head(dfType)
```
Nous avons ici appliqué un filtre afin de sélectionner les profils ayant le pourcentage le plus eleve dans notre jeu de données en Europe. 
On rappelle nos résultats trouvés jusqu'ici:
- M 
- 25-29 
- ED3_4 
- SM-DAY

Nos résultats étant des données calculés par valeur moyenne, nous retrouvons d'autres variable avec des tranches d'âge plus élevé avec au premier rang :
- M
- 18-44
- ED0-2
- SM-DAY

Ainsi on observe que nos résultats concordent avec les observations précédentes ci-dessus sans exception notable. 

## IV Conclusion

En conclusion, nous avons pu remarquer l'influence des différents facteurs (Sexe, Âge, Niveau d'étude et Localisation) avec leurs taux de consommation de tabac en Europe:

- les Hommes sont plus consommateur de tabac que les Femmes
- Plus l'âge augmenté plus la consommation de tabac diminue
- Plus le niveau d'étude est élevé plus la consommation diminie
- Les pays ont leur propre influence sur la consommation de leurs populations
- Les fumeurs d'Europe sont majoritairement des fumeurs quotidiens

## V Bonus

Pour répondre à l'une de nos interrogations du début, nous souhaitions expliquer la présence de fumeurs de plus en plus précoce:

### l'age en fonction de la consomation de tabac en France
```{r}
testdf <- subset(dfwu, grepl("_D|_O", dfwu$smoking))
testdf <- subset(testdf, grepl("TOTAL", dfwu$isced11))
testdf <- subset(testdf, grepl("_|-", dfwu$age))
testdf <- subset(testdf, grepl("M|F", testdf$sex))


testdf <- testdf[complete.cases(testdf), ]

index <- with(testdf, order(-FR))
testdf<-data.frame(Smoking=testdf[2][index, ],Isced11=testdf[3][index, ],Sex=testdf[4][index, ],Age=testdf[5][index, ],FR = testdf[17][index, ])
testdf$rank <- rank(-testdf$FR)

testdf <- subset(testdf, grepl("Y15-", testdf$Age))

testdf

```

Et en effet, on observe le classement des plus de 15 ans beaucoup plus haut que la moyenne Européenne avec des tranches 15-? dans la première moitié du classement (sur 80 rangs au total).

