# Mini-Projet

The Mini-Projet has to be developed in *groups of two students*.

## Datasets

List where data sets have to be chosen from:

- FR Data.gouv: https://www.data.gouv.fr/fr/
- Insee: https://www.insee.fr/fr/accueil
- Kaggle: https://www.kaggle.com/datasets
- US Data.gov: https://catalog.data.gov/dataset


## Guidelines

1. Justify the data set selection and described it
2. Formulate three questions about the selected data set
3. Discuss with the professors and choose one question among these
4. Propose a methodology to answer that question
5. Implement this methodology using Literate Programming
6. The report must be acessible online (in your git repository)

## Report

The R Markdown (to be written in RStudio) must contain:

- Frontpage
  - _Title_
  - Student's name
- Table of contents
- Introduction
  - Context / Dataset description
    - How the dataset has been obtained?
  - Description of the question
- Methodology
  - Data clean-up procedures
  - Scientific workflow
  - Data representation choices
- Analysis in Literate Programming
- Conclusion
- References

## Important notes

The question might be from simple to complex. More the question is
simple, more deep should be the analysis.

## Groups

| id | Group | Teams                                  |   Repository
|----|-------|----------------------------------------|-------------------------
| 1  |  G2   | Guillaume Metzger / Thomas Gevara      | https://gricad-gitlab.univ-grenoble-alpes.fr/metzgegu/MSPL
| 2  |  G2   | Jules Noel / Gabriel Nourrit           | https://gricad-gitlab.univ-grenoble-alpes.fr/noeljul/MSPL
| 3  |  G2   | Julien Alaimo / Quentin Stehlin        | https://gricad-gitlab.univ-grenoble-alpes.fr/alaimoj/MSPL
| 4  |  G2   | Mouataz Amal / Léo Bello               | https://gricad-gitlab.univ-grenoble-alpes.fr/amalm/MSPL
| 5  |  G2   | Tom Barthelemy / Seif Boubaker         | https://gricad-gitlab.univ-grenoble-alpes.fr/barthelt/MSPL
| 6  |  G2   | Yanan Lyu / Jefferson Cessna           | https://gricad-gitlab.univ-grenoble-alpes.fr/lyuya/MSPL
| 7  |  G2   | Melanie Chasane / Céline Botté         | https://gricad-gitlab.univ-grenoble-alpes.fr/chasanem/MSPL
| 8  |  G2   | Aminata Ba / Antoine Carrier           | https://gricad-gitlab.univ-grenoble-alpes.fr/baa/MSPL
| 9  |  G2   | Sébastien Lempreur / Rivana Alvano     | https://gricad-gitlab.univ-grenoble-alpes.fr/lemperes/MSPL
| 10 |  G2   | Zakaria Choukchou Braham               | https://gricad-gitlab.univ-grenoble-alpes.fr/choukchz/MSPL
| 11 |  G2   | Zouhayr Aklouh / Amine Beluziane       | https://gricad-gitlab.univ-grenoble-alpes.fr/aklouhz/MSPL
| 12 |  G2   | Jimmy Chevalier / Michael Koc          | https://gricad-gitlab.univ-grenoble-alpes.fr/chevajim/MSPL
| 13 |  G2   | Noureddine Ziani / Abdelnour Sassi     | https://gricad-gitlab.univ-grenoble-alpes.fr/zianinou/MSPL
| 14 |  G2   | Balle Traore Loic Ramphort             | https://gricad-gitlab.univ-grenoble-alpes.fr/traoreba/MSPL

